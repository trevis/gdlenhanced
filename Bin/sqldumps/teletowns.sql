-- phpMyAdmin SQL Dump
-- version 4.4.15.8
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 01, 2017 at 10:32 AM
-- Server version: 5.6.31
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gdle`
--

-- --------------------------------------------------------

--
-- Dumping data for table `teletowns`
--

INSERT INTO `teletowns` (`ID`, `Description`, `Command`, `Landblock`, `Position_X`, `Position_Y`, `Position_Z`, `Orientation_W`, `Orientation_X`, `Orientation_Y`, `Orientation_Z`) VALUES
(52,'Teletown - Eastwatch','Eastwatch','49F00023','42d80000','42700000','432a0000','3f800000','00000000','00000000','00000000'),
(53,'Teletown - Westwatch','Westwatch','23DA002B','430535c3','428ce148','4000a3d7','bf7d70a4','00000000','00000000','00000000'),
(54,'Teletown - Silyun','Silyun','27EC0015','42700000','42d80000','42a0051f','3f800000','00000000','00000000','00000000'),
(55,'Teletown - Sanamar','Sanamar','33D9000B','423b51ec','42687ae1','42500a3d','3f800000','00000000','00000000','3f800000'),
(56,'Teletown - Merwart Village','Merwart Village','C9E3000B','42100000','2700000','41600000','3f800000','00000000','00000000','bf800000'),
(57,'Teletown - Fiun Outpost','Fiun Outpost','38F7001A','42a0e666','41fce148','3faf5c29','00000000','00000000','00000000','3f800000'),
(58,'Teletown - Kor-Gursha','Kor-Gursha','009C0247','43200000','c28c0000','c1900000','3f800000','00000000','00000000','bf800000'),
(59,'Teletown - Mar\'uun','Mar\'uun','880401C4','42200000','c3020000','c3700000','3f800000','00000000','00000000','00000000');

